﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaslavskyiMykola.RobotChallange;
using Robot.Common;

namespace PaslavskyiMykolaRobotChallange.Test
{
    [TestClass]
    public class TestDirection
    {
        [TestMethod]
        public void TestAllDirections()
        {
            var position = new Position(5,5);
            var newposition = new Position(4,6);
            var position2 = Direction.allDirections(position, 1);
            Assert.AreEqual(position2, newposition);
            Assert.AreNotEqual(position,position2);

        } 
        [TestMethod]
        public void TestMoveOneStep()
        {
            var start = new Position(5,5);
            var finish = new Position(7,8);
            var position = Direction.MoveOnOneStepInDirection(start, finish);
            while (position!= finish)
            {
                position = Direction.MoveOnOneStepInDirection(position, finish);
            }
            Assert.AreEqual(finish, position);

        }
    }
}
