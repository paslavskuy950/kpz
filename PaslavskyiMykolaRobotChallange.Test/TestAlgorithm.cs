﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaslavskyiMykola.RobotChallange;
using Robot.Common;

namespace PaslavskyiMykolaRobotChallange.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestDoMove()
        {
            var algorithm = new PaslavskyiMykolaAlgorithm();
            var pRobot = new Position(2, 3);
            var robot = new Robot.Common.Robot() {Energy = 500, Position = pRobot};
            var p1 = new Position(4, 6);
            var newPosition = algorithm.DoMove(robot, 2, 3); //4 6
            var robotPos = robot.Position;
            Assert.IsTrue(newPosition is Position);
            Assert.AreEqual(p1, newPosition);
            Assert.AreEqual(newPosition,robotPos);
        }
        [TestMethod]
        public void TestMoveToPosotion()
        {
            var algorithm = new PaslavskyiMykolaAlgorithm();
            var pRobot = new Position(2, 3);
            var robot = new Robot.Common.Robot() {Energy = 500, Position = pRobot};
            var p1 = new Position(4, 6);
            var newPosition = algorithm.MoveToPosition(robot, p1); //4 6
            var robotPos = robot.Position;
            Assert.IsTrue(newPosition is Position);
            Assert.AreEqual(p1, newPosition);
            Assert.AreEqual(newPosition,robotPos);
        }
        [TestMethod]
        public void TestStayAtSamePlace()
        {
            var algorithm = new PaslavskyiMykolaAlgorithm();
            var pRobot = new Position(2, 3);
            var robot = new Robot.Common.Robot() {Energy = 500, Position = pRobot};
            var p1 = new Position(2, 3);
            var newPosition = algorithm.StayAtSamePlace(robot); //4 6
            Assert.IsTrue(newPosition is Position);
            Assert.AreEqual(p1, newPosition);
        }
        [TestMethod]
        public void TestMoveToStation()
        {
            var algorithm = new PaslavskyiMykolaAlgorithm();
            var pRobot = new Position(2, 3);
            var pStation1 = new Position(4, 6);
            var pStation2 = new Position(0, 1);
            var map = new Map();
            var station1 = new EnergyStation() { Position = pStation1 };
            var station2 = new EnergyStation() { Position = pStation2 };
            map.Stations.Add(station1);
            map.Stations.Add(station2);
            var robotList = new List<Robot.Common.Robot>();
            var robot = new Robot.Common.Robot() {Energy = 500, Position = pRobot};
            robotList.Add(robot);
            var p1 = new Position(0, 1);
            var newPosition = algorithm.MoveToStation(robot, DistanceHelper.FindNearestStation(robot.Position, map)); //0 1
            Assert.IsTrue(newPosition is Position);
            Assert.AreEqual(p1, newPosition);
            Assert.AreEqual(robot.Position, station2.Position);
        }
        [TestMethod]
        public void TestAttackRobot()
        {
            var algorithm = new PaslavskyiMykolaAlgorithm();
            var pRobot1 = new Position(2, 3);
            var pRobot2 = new Position(4, 5);

            var robot1 = new Robot.Common.Robot() { Energy = 500, Position = pRobot1, OwnerName = "Mykola" };
            var robot2 = new Robot.Common.Robot() { Energy = 500, Position = pRobot2, OwnerName = "Tanya" };
            var p1 = new Position(4, 5);
            algorithm.AttackRobot(robot1, robot2); //0 1
            Assert.AreEqual(p1, robot1.Position);
            Assert.AreEqual(robot1.Position, robot2.Position);
        }
    }
}
