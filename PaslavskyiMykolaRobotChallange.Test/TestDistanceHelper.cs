﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaslavskyiMykola.RobotChallange;
using Robot.Common;

namespace PaslavskyiMykolaRobotChallange.Test
{
    [TestClass]
    public class TestDistanceHelper
    {
        [TestMethod]
        public void TestCountEnergyToMove()
        {
            var p1 = new Position(1,1);
            var p2 = new Position(1,4);
            var command = DistanceHelper.CountEnergyToMove(p1, p2);
            Assert.AreEqual(9, command);
        }
        [TestMethod]
        public void TestIsCellFree()
        {
            var p1 = new Position(1,1);
            var p2 = new Position(1,4);
            var freeCell = new Position(1,2);
            var robot = new Robot.Common.Robot(){Energy = 500, Position = p1};
            var movingRobot = new Robot.Common.Robot(){Energy = 400, Position = p2};
            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            robots.Add(movingRobot);

            var command = DistanceHelper.IsCellFree(freeCell,movingRobot,robots);
            Assert.IsTrue(command);
        }        
        [TestMethod]
        public void TestIsStationFree()
        {
            var pStation = new Position(3,3);
            var pRobot = new Position(2,2);
            var pMovingRobot = new Position(1,1);

            var station = new EnergyStation(){Position = pStation};
            
            var robot = new Robot.Common.Robot(){Energy = 500, Position = pRobot};
            var movingRobot = new Robot.Common.Robot(){Energy = 400, Position = pMovingRobot};
            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            robots.Add(movingRobot);

            var isFreeStation = DistanceHelper.IsStationFree(station, movingRobot, robots);
            Assert.IsTrue(isFreeStation);
        }
        [TestMethod]
        public void TestFindNearestStation()
        {
            var map = new Map();
            var p1 = new Position(3, 3);
            var p2 = new Position(2, 2);
            var p3 = new Position(1, 1);

            map.Stations.Add(new EnergyStation() { Position = p1 });
            map.Stations.Add(new EnergyStation() { Position = p2 });
            map.Stations.Add(new EnergyStation() { Position = p3 });

            var pRobot1 = new Position(3,3);
            var pRobot2 = new Position(5,5);

            var robot = new Robot.Common.Robot() { Energy = 500, Position = pRobot1 };
            var movingRobot = new Robot.Common.Robot() { Energy = 400, Position = pRobot2 };
            var robots = new List<Robot.Common.Robot>();
            robots.Add(robot);
            robots.Add(movingRobot);

            var nearestStation = DistanceHelper.FindNearestStation(movingRobot.Position, map);
            Assert.AreEqual(p2,nearestStation);
        }       
        [TestMethod]
        public void TestMostEnergyStation()
        {
            var map = new Map();
            var p1 = new Position(3, 3);
            var p2 = new Position(2, 2);
            var p3 = new Position(1, 1);

            map.Stations.Add(new EnergyStation() { Position = p1,Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p2,Energy = 1000});
            map.Stations.Add(new EnergyStation() { Position = p3, Energy = 700});

            var maxEnergyStation = DistanceHelper.FindMostEnergyStation(map);
            Assert.AreEqual(p2,maxEnergyStation);
        }        
        [TestMethod]
        public void TestGetPositionsWhereToGo()
        {
            var map = new Map();
            var p1 = new Position(8, 8);
            var p2 = new Position(8, 6);
            var p3 = new Position(6, 7);
            var p4 = new Position(5, 5);
            var p5 = new Position(6, 4);
            var p6 = new Position(11, 6);
            var p7 = new Position(4, 10);
            var p8 = new Position(6, 9);
            var p9 = new Position(6, 12);

            map.Stations.Add(new EnergyStation() { Position = p1,Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p2,Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p3, Energy = 500 });
            map.Stations.Add(new EnergyStation() { Position = p4, Energy = 500 });
            map.Stations.Add(new EnergyStation() { Position = p5, Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p6, Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p7, Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p8, Energy = 500});
            map.Stations.Add(new EnergyStation() { Position = p9, Energy = 500});
            
            var robot = new Robot.Common.Robot(){Energy = 400,Position = new Position(10,10)};

            var positionToGo = DistanceHelper.GetPositionWhereToGo(robot.Position, map, 2);
            var expectedPosition = new Position(6,6);
            Assert.AreEqual(expectedPosition,positionToGo);
        }        
              

    }
}
