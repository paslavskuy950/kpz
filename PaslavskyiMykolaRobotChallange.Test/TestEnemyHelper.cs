﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaslavskyiMykola.RobotChallange;
using Robot.Common;

namespace PaslavskyiMykolaRobotChallange.Test
{
    [TestClass]
    public class TestEnemyHelper
    {
        [TestMethod]
        public void TestFindMostEnergyEnemy()
        {
            var p1 = new Position(3, 3);
            var p2 = new Position(2, 2);
            var p3 = new Position(1, 1);
            var p4 = new Position(4, 4);
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot { Energy = 500, OwnerName = "Mykola", Position = p1 });
            robots.Add(new Robot.Common.Robot { Energy = 900, OwnerName = "Mykola", Position = p2 });
            robots.Add(new Robot.Common.Robot { Energy = 600, OwnerName = "Tanya", Position = p3 });
            robots.Add(new Robot.Common.Robot { Energy = 800, OwnerName = "Tanya", Position = p4 });
            var greatestEnemy = EnemyHelper.FindMostEnergyEnemy(robots[0], robots);
            Assert.AreEqual(p4, greatestEnemy);
        }
        [TestMethod]
        public void TestFindMostEnergyEnemyInRange()
        {
            var p1 = new Position(3, 3);
            var p2 = new Position(2, 2);
            var p3 = new Position(0, 0);
            var p4 = new Position(4, 4);
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot { Energy = 500, OwnerName = "Mykola", Position = p1 });
            robots.Add(new Robot.Common.Robot { Energy = 900, OwnerName = "Mykola", Position = p2 });
            robots.Add(new Robot.Common.Robot { Energy = 600, OwnerName = "Tanya", Position = p3 });
            robots.Add(new Robot.Common.Robot { Energy = 400, OwnerName = "Tanya", Position = p4 });
            var greatestEnemy = EnemyHelper.FindMostEnergyEnemyInRange(robots[0], robots,15);
            Assert.AreEqual(p4, greatestEnemy);
            greatestEnemy = EnemyHelper.FindMostEnergyEnemyInRange(robots[0], robots,18);
            Assert.AreEqual(p3, greatestEnemy);
        }

        [TestMethod]
        public void TestFindNearestEnemy()
        {
            var p1 = new Position(3, 3);
            var p2 = new Position(2, 2);
            var p3 = new Position(1, 1);
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot { Energy = 500, OwnerName = "Mykola", Position = p1 });
            robots.Add(new Robot.Common.Robot { Energy = 500, OwnerName = "Mykola", Position = p2 });
            robots.Add(new Robot.Common.Robot { Energy = 500, OwnerName = "Tanya", Position = p3 });


            var nearestEnemy = EnemyHelper.FindNearestEnemy(robots[0], robots);
            Assert.AreEqual(p3, nearestEnemy);
        }
    }
}
