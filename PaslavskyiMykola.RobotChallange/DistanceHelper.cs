﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using Robot.Common;

namespace PaslavskyiMykola.RobotChallange
{
    public class DistanceHelper
    {
        public static int CountEnergyToMove(Position p1, Position p2)
        {
            int energy = (int) (Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
            return energy;
        }
        public static bool IsInRadius(Position p1, Position p2, int radius)
        {
            int distance = radius * radius * 2;
            if ((int) (Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2)) <= distance)
                return true;
            return false;
        }
        public static Position FindNearestStation(Position position, Map map)
        {
            int distance = 0;
            int minDistance = int.MaxValue;
            EnergyStation nearest = null;
            foreach (var station in map.Stations)
            {

                distance = CountEnergyToMove(position, station.Position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    nearest = station;
                }
            }
            return nearest.Position;
        }
        public static int NumberStationsInRange(Position position, Map map, int radius)
        {
            int k = 0;
            foreach (var station in map.Stations)
            {
                if (IsInRadius(position, station.Position, radius))
                {
                    k++;
                }
            }
            return k;
        }     

        public static int NumberOfEnergyInRange(Position position, Map map, int radius)
        {
            int energy = 0;
            foreach (var station in map.Stations)
            {
                if (IsInRadius(position, station.Position, radius))
                {
                    energy += station.Energy;
                }
            }
            return energy;
        }


        public static Position GetPositionWhereToGo(Position position, Map map, int radius)
        {

            var nearest = FindNearestStation(position, map);
            int n = 4;
            IList<int> numOfStations = new List<int>(n);
            IList<int> numOfEnergy = new List<int>(n);
            IList<Position> positions = new List<Position>(n);
            positions.Add(Direction.GoLeftTop(nearest, radius));
            positions.Add(Direction.GoRightTop(nearest, radius));
            positions.Add(Direction.GoRightBottom(nearest, radius));
            positions.Add(Direction.GoLeftBottom(nearest, radius));

            for (int i = 0; i < n; ++i)
            {
                numOfStations.Add(NumberStationsInRange(positions[i], map, radius));
                numOfEnergy.Add(NumberOfEnergyInRange(positions[i], map, radius));
            }

            var maxEnergy = numOfEnergy.Max();
            var maxEnergyIndex = numOfEnergy.IndexOf(maxEnergy);
            var maxStations = numOfStations.Max();
            var maxStationsIndex = numOfStations.IndexOf(maxStations);
            if (maxEnergyIndex == maxStationsIndex)
                return positions[maxStationsIndex];
            else
            {
                return CalculateProfit(position, positions[maxEnergyIndex], positions[maxStationsIndex],
                    maxEnergy, maxStations, numOfEnergy[maxStationsIndex], numOfStations[maxEnergyIndex]);
            }
        }
        public static Position CalculateProfit(Position position, Position energyPos, Position stationsPos,
            int maxEnergy,int maxStations, int minEnergy, int minStations)
        {
            int stationProfit = 20;
            var distance1 = CountEnergyToMove(position, energyPos);
            var distance2 = CountEnergyToMove(position, stationsPos);
            var profitFromCollect1 = maxEnergy / minStations;
            if (profitFromCollect1 > 200)
                profitFromCollect1 = 200;

            var profitFromCollect2 = minEnergy / maxStations;
            if (profitFromCollect2 > 200)
                profitFromCollect2 = 200;

            int profit1 = profitFromCollect1 + stationProfit * minStations - distance1;
            int profit2 = profitFromCollect2 + stationProfit * maxStations - distance2;

            if (profit1 > profit2)
                return energyPos;
            else
                return stationsPos;
        }



        public static Position FindMostEnergyStation(Map map)
        {
            int energy = 0;
            int maxEnergy = int.MinValue;
            EnergyStation maxEnergyStation = null;
            foreach (var station in map.Stations)
            {
                energy = station.Energy;
                if (energy > maxEnergy)
                {
                    maxEnergy = energy;
                    maxEnergyStation = station;
                }
            }
            return maxEnergyStation.Position;
        }

        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public static bool IsCellFree(Position position, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if(robot != movingRobot)
                {
                    if (robot.Position == position)
                        return false;
                }
            }
            return true;
        }
    }

        
}
