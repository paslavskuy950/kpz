﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaslavskyiMykola.RobotChallange
{
    public class EnemyHelper
    {
        public static Position FindNearestEnemy(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            int distance = 0;
            int minDistance = int.MaxValue;
            Robot.Common.Robot nearest = null;
            foreach (var robot in robots)
            {
                if (robot.OwnerName != movingRobot.OwnerName)
                {
                    distance = DistanceHelper.CountEnergyToMove(movingRobot.Position, robot.Position);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        nearest = robot;
                    }
                }
            }
            return nearest.Position;
        }

        public static Position FindMostEnergyEnemy(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            int energy = 0;
            int maxEnergy = int.MinValue;
            Robot.Common.Robot greatest = null;
            foreach (var robot in robots)
            {
                if (robot.OwnerName != movingRobot.OwnerName)
                {
                    energy = robot.Energy;
                    if (energy > maxEnergy)
                    {
                        maxEnergy = energy;
                        greatest = robot;
                    }
                }
            }
            return greatest.Position;
        }
        public static Position FindMostEnergyEnemyInRange(Robot.Common.Robot movingRobot,
                IList<Robot.Common.Robot> robots, int distance)
        {
            IList<Robot.Common.Robot> robotsInRange = new List<Robot.Common.Robot>(5);
            foreach (var robot in robots)
            {
                if (DistanceHelper.CountEnergyToMove(movingRobot.Position, robot.Position) <= distance) 
                    robotsInRange.Add(robot);
            }
            return FindMostEnergyEnemy(movingRobot, robotsInRange);
        }

    }
}
