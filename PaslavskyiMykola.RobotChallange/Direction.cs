﻿using System;
using System.Collections.Generic;
using System.Text;
using Robot.Common;

namespace PaslavskyiMykola.RobotChallange
{
    public class Direction
    {

        public static Position MoveOnOneStepInDirection(Position start, Position finish)
        {
            var position = new Position(start.X,start.Y);
            var x = finish.X - start.X;
            var y = finish.Y - start.Y;
            if (x == 0)
            {
                if (y < 0)
                    position = GoTop(start, 1);
                else
                    position = GoBottom(start, 1);
            }
            else if (y == 0)
            {
                if (x < 0)
                    position = GoLeft(start, 1);
                else
                    position = GoRight(start, 1);
            }
            else if (x < 0 && y < 0)
                position = GoLeftTop(position, 1);
            else if (x < 0 && y > 0)
                position = GoLeftBottom(position, 1);
            else if (x > 0 && y < 0)
                position = GoRightTop(position, 1);
            else if (x > 0 && y > 0)
                position = GoRightBottom(position, 1);

            return position;
        }public static Position GoLeft(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition.X -= step;
            return newposition;
        }
        public static Position GoRight(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition.X += step;
            return newposition;
        }
        public static Position GoTop(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition.Y -= step;
            return newposition;
        }
        public static Position GoBottom(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition.Y += step;
            return newposition;
        }
        public static Position GoLeftTop(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition = GoLeft(position, step);
            newposition = GoTop(newposition, step);
            return newposition;
        }
        public static Position GoRightTop(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition = GoRight(position, step);
            newposition = GoTop(newposition, step);
            return newposition;
        }
        public static Position GoRightBottom(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition = GoRight(position, step);
            newposition = GoBottom(newposition, step);
            return newposition;
        }
        public static Position GoLeftBottom(Position position, int step)
        {
            var newposition = new Position(position.X, position.Y);
            newposition = GoLeft(position, step);
            newposition = GoBottom(newposition, step);
            return newposition;
        }
        public static Position allDirections(Position position, int step)
        {
            var newposition = GoLeftTop(position, step);
            newposition = GoRightTop(newposition, step);
            newposition = GoRightBottom(newposition, step);
            newposition = GoLeftBottom(newposition, step);
            newposition = GoLeftBottom(newposition, step);
            return newposition;
        }


    }
}
