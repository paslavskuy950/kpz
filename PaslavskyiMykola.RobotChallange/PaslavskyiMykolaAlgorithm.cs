﻿using Robot.Common;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;

namespace PaslavskyiMykola.RobotChallange
{
    public class PaslavskyiMykolaAlgorithm : IRobotAlgorithm
    {
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var myRobot = robots[robotToMoveIndex];
            var finishPosition = DistanceHelper.GetPositionWhereToGo(myRobot.Position, map, 2);
            var positionToMove = finishPositionToMove(myRobot, finishPosition);
            if (positionToMove == finishPosition)
                return new CollectEnergyCommand();
            else
                return new MoveCommand() { NewPosition = positionToMove };

        }

        public Position DoMove(Robot.Common.Robot robot, int x, int y)
        {
            //Add x and y to old position
            robot.Position.X += x;
            robot.Position.Y += y;
            return robot.Position;
        }
        public Position MoveToPosition(Robot.Common.Robot robot, Position position)
        {
            //Move to new position
            robot.Position = position;
            return robot.Position;
        }
        public Position MoveToStation(Robot.Common.Robot robot, Position position)
        {
            robot.Position = position;
            return robot.Position;
        }
        public void AttackRobot(Robot.Common.Robot yourRobot, Robot.Common.Robot enemyRobot)
        {
            yourRobot.Position = new Position(enemyRobot.Position.X, enemyRobot.Position.Y);
        }
        public Position StayAtSamePlace(Robot.Common.Robot robot)
        {
            return DoMove(robot, 0, 0);
        }

        public Position finishPositionToMove(Robot.Common.Robot robot, Position position)
        {
            int distance = 0;
            int energyToSpend = 30;
            var finishPosition = new Position(robot.Position.X, robot.Position.Y);
            int sumOfEnergy = 0;
            //while (finishPosition != position)
            //{
            //    if (sumOfEnergy > robot.Energy)
            //    {
            //        sumOfEnergy = 0;
            //        --energyToSpend;
            //        finishPosition = new Position(robot.Position.X, robot.Position.Y);
            //    }
            //    while (distance <= energyToSpend)
            //    {
            //        finishPosition = Direction.MoveOnOneStepInDirection(finishPosition, position);
            //        distance = DistanceHelper.CountEnergyToMove(robot.Position, finishPosition);
            //    }
            //    sumOfEnergy += distance;
            //}
            for (int i = 0; i < 10; ++i)
            {
                var tempPosition = new Position(finishPosition.X, finishPosition.Y);
                finishPosition = Direction.MoveOnOneStepInDirection(robot.Position, position);
                distance = DistanceHelper.CountEnergyToMove(robot.Position, finishPosition);
                if (finishPosition == position)
                    break;
                else if (distance >= energyToSpend)
                {
                    finishPosition = tempPosition;
                    break;
                }
            }

            return finishPosition;
        }



        public string Author
        {
            get { return "Paslavskyi Mykola"; }
        }
    }
}
